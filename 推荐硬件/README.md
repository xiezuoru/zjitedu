# 推荐硬件

教材支持microbit v1、microbit v2和掌控板。因为microbit不支持Wi-Fi，因而推荐掌控板。

## 掌控板介绍

一款为普及创客教育而生的开源硬件，价格低廉而性能强大，支持Wi-Fi功能并集成了按钮、触摸键、麦克风、蜂鸣器和显示屏等常见电子模块，适用于中小学的大班教学。掌控板默认支持MQTT协议，为物联网项目设计而生。

## 掌控板购买地址

1.DF商城

https://www.dfrobot.com.cn/goods-1902.html

2.微信商城

微信上搜索“掌控板”，进入公众号即可购买。

3.淘宝商城

淘宝上搜索“掌控板”即可。

https://item.taobao.com/item.htm?id=589150302447


## 掌控板学习社区

掌控板有多个网络社区，规模较大的有盛思（labplus）和DFRobot组织。

1.labplus

地址：https://www.labplus.cn/forum

2.DF社区

地址：https://mc.dfrobot.com.cn/

## 相关软件下载

1.BXY Editor

https://bxy.dfrobot.com.cn/download

2.mPython

https://www.mpython.cn/
