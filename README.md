# 项目说明

本仓库提供了浙江教育出版社普通高中信息教材的配套资源。

由于精力关系，目前仅提供必修2模块《信息系统与社会》和《开源硬件项目设计》的开源硬件部分代码。

## 其他下载方式

百度网盘：

## 相关软件下载

1.BXY Editor

https://bxy.dfrobot.com.cn/download

2.SIoT

https://mindplus.dfrobot.com.cn/siot

## 电子教材下载：

[国家中小学智慧教育平台](https://basic.smartedu.cn/elecEdu)