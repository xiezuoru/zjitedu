'''
P61 掌控板程序
'''
from microbit import *

while True:
    if uart.any():
        incoming=str(uart.read(1),"UTF-8")
        incoming=incoming.strip('\n')
        if incoming=='H':
            display.show(Image.HAPPY)
            uart.write("I am happy\r\n")
        elif incoming=='S':
            display.show(Image.SAD)
            uart.write("I am sad\r\n")
        else:
            uart.write("err\r\n")