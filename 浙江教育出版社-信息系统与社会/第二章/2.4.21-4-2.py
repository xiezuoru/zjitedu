# 交互式串口程序，发送字符并显示返回信息
import serial,time
ser = serial.Serial()
ser.baudrate = 115200
ser.port = 'COM3'
ser.open()
while True:
    name=input()
    ser.write(name.encode())
    line = ser.readline()
    print(line.strip().decode())