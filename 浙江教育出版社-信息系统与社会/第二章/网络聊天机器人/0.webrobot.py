from flask import Flask, render_template       #导入网页模板支持模块,重点
#from flask_script import Server, Manager  书上此导入无用
from flask_bootstrap import Bootstrap          #导入网页模板工具模块
#Bootstrap是Twitter推出的一个用于前端开发的开源工具包
#from flask_moment import Moment     书上此导入无用
from flask_wtf import FlaskForm                #导入表单模块
from wtforms import StringField, SubmitField   #导入表单文本框，提交按钮模块
from wtforms.validators import  InputRequired  #导入表单输入验证模块

import sys
sys.path.insert(0, "../")

import aiml                #导入人工智能标记语言aiml模块 
k = aiml.Kernel()          #创建aiml聊天机器人
k.learn("cn-startup.xml")  #读取语料库
k.respond("load aiml cn")  #机器人回应
k.respond("start")         #机器人回应
                         
app = Flask(__name__)      #创建Flask实例app，重点
app.config['SECRET_KEY'] = 'hard to guess string'
#设置信息传输密钥，一般设置24位字符

bootstrap = Bootstrap(app)  #实例化Bootstrap类,
#moment = Moment(app)    书上此语句无用

# 谢作如增加代码
import json
def savecsv(sms):
    with open('sms.csv', 'wb') as f:
        for item in sms:
            f.write(str(item).encode("utf-8"))
        # 另一种方式
        #f.writelines(t.encode("utf-8")) 

def readcsv():
    with open('sms.csv', 'r',encoding='utf-8') as f:
        j = f.readlines()
    return j

class NameForm(FlaskForm):                  #创建网页表单类
    name = StringField('请开始交谈：', validators=[InputRequired()])  #文本输入框name
    submit = SubmitField('提交')            #提交按钮submit

@app.route('/', methods=['GET', 'POST'])  #设置根路由，请求方式允许GET或POST，若不写，默认是GET请求
def index():   #与路由匹配的视图函数index，函数名自定义，不冲突即可
    name = r = ''
    #sms = []
    sms = readcsv()
    form = NameForm()                 #创建表单实例form
    if form.validate_on_submit():     #如果按钮被单击
        name = form.name.data     #hello      #读取文本框name中的数据赋值给变量name
        print(name)
        sms.append(name)
        r = k.respond(name)
        print(r)
        sms.append(r)
        savecsv(sms)
        form.name.data = ''           #清空文本框name的数据
    return render_template('index.html', form=form, name=r, sms=sms)
    #打开index.html网页的同时传送表单和机器人回应的内容

if __name__ == '__main__':   #程序运行入口
    app.run()
