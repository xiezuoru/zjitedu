# 交互式串口程序，发送字符并显示返回信息
import serial,time
ser = serial.Serial()
ser.baudrate = 115200
ser.port = 'COM3'
ser.open()
time.sleep(3)  # 等待掌控板完成串口对接
print('系统数据:\n',ser.read(ser.in_waiting).decode())
while True:
    name=input()
    ser.write(name.encode())
    line = ser.readline()
    print(line.strip().decode())