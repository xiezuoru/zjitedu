# 资源说明

文件名中加上m标识的，是运行在开源硬件上的代码。在microbit和掌控板上都可以正常运行。

文件名中加上h标识的，是运行在掌控板上的代码。

其他文件运行在电脑上，需要安装pyserial库。

安装方式：pip install pyserial

## 注意事项

1.当BXY或者mPython（硬件模式）正在运行时，再运行电脑端端python文件，容易造成端口冲突，建议先断开端口或者关闭BXY和mPython软件。

2.掌控板支持的功能更多，建议使用BXY，可以看到各种参考代码。

掌控板的MicroPython文档：

https://mpython.readthedocs.io/zh/master/

教材配套软件BXY的说明文档：https://bxy.dfrobot.com.cn/xxjs-mpython


