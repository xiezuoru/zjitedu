# 读取串口信息显示表情，并输出文字
# 如果使用掌控板，建议将print改为uart.write
from microbit import *
while True:
    if uart.any():
        incoming = str(uart.readall(), "UTF-8")
        incoming=incoming.strip('\r')
        if incoming=='H':
            display.show(Image.HAPPY)
            print("I am happy")
        elif incoming=='S':
            display.show(Image.SAD)
            print("I am sad")
        else:
            print("err")