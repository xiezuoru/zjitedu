# 读取传感器编号为1的json数据，并绘图
# 换成了request库
import sys, requests, json
import pandas as pd
import matplotlib.pyplot as plt
def showpic(r):
    #print(content)
    data = r.json()
    #节点可以这样读出
    print(data["sensorid"])
    val=data["value"]
    #print(val[0])
    df=pd.DataFrame(val)
    #print(df)
    #绘图，默认是折线图
    ax=df.plot()
    #显示图形 
    plt.show()

url = "http://192.168.3.41:8080/get?id=1"
r = requests.get(url) #访问
data = r.json() #将JSON转化为字典
maxs=1
t=1
v_list=data["value"]
print(len(v_list))
for i in range(1,len(v_list)):
    print(i)
    if v_list[i]["sensorvalue"] > v_list[i-1]["sensorvalue"]:
        t += 1
    else:
        t = 1           
    if t >= maxs:
        maxs = t
        p = i
print(p,maxs)
print("温度持续上升的最长时间段是：")
# 列表是从0开始编号的
print("从%s到%s。"%(v_list[p-maxs+1]["updatetime"],v_list[p+1]["updatetime"]))