# 直接读json数据
import requests,json
import matplotlib.pyplot as plt
def showpic(r):
    #print(content)
    data = r.json()
    #节点可以这样读出
    print(data["sensorid"])
    val=data["value"]
    #print(val[0])
    df=pd.DataFrame(val)
    #print(df)
    #绘图，默认是折线图
    ax=df.plot()
    #显示图形 
    plt.show()

# url = 'http://192.168.3.41:8081/get?id=1'
# r = requests.get(url) #访问
# data = r.json() #将JSON转化为字典
r = '''{"value": [{"updatetime": "2017-07-18 6:10:23", "sensorvalue": 19.09},
 {"updatetime": "2017-07-18 6:40:28", "sensorvalue": 19.19},
 {"updatetime": "2017-07-18 7:10:33", "sensorvalue": 20.11},
 {"updatetime": "2017-07-18 7:40:10", "sensorvalue": 20.12},
 {"updatetime": "2017-07-18 8:10:48", "sensorvalue": 21.13},
 {"updatetime": "2017-07-18 8:40:15", "sensorvalue": 22.14},
 {"updatetime": "2017-07-18 9:10:27", "sensorvalue": 23.14},
 {"updatetime": "2017-07-18 9:40:37", "sensorvalue": 23.50},
 {"updatetime": "2017-07-18 10:10:05", "sensorvalue": 24.14},
 {"updatetime": "2017-07-18 10:40:28", "sensorvalue": 25.15},
 {"updatetime": "2017-07-18 11:10:42", "sensorvalue": 26.56},
 {"updatetime": "2017-07-18 11:40:57", "sensorvalue": 28.17},
 {"updatetime": "2017-07-18 12:10:06", "sensorvalue": 30.18},
 {"updatetime": "2017-07-18 12:40:34", "sensorvalue": 31.19},
 {"updatetime": "2017-07-18 13:10:46", "sensorvalue": 32.20},
 {"updatetime": "2017-07-18 13:40:01", "sensorvalue": 32.20},
 {"updatetime": "2017-07-18 14:10:18", "sensorvalue": 32.10},
 {"updatetime": "2017-07-18 14:40:39", "sensorvalue": 32.10},
 {"updatetime": "2017-07-18 15:10:52", "sensorvalue": 31.23},
 {"updatetime": "2017-07-18 15:40:47", "sensorvalue": 31.24},
 {"updatetime": "2017-07-18 16:10:08", "sensorvalue": 31.10},
 {"updatetime": "2017-05-18 16:40:48", "sensorvalue": 30.25},
 {"updatetime": "2017-05-18 17:10:44", "sensorvalue": 28.26},
 {"updatetime": "2017-05-18 17:40:57", "sensorvalue": 27.27},
 {"updatetime": "2017-05-18 18:10:29", "sensorvalue": 27.20},
 {"updatetime": "2017-05-18 19:40:23", "sensorvalue": 25.29},
 {"updatetime": "2017-05-18 20:10:42", "sensorvalue": 24.3},
 {"updatetime": "2017-05-18 20:40:00", "sensorvalue": 24.1},
 {"updatetime": "2017-05-18 21:10:28", "sensorvalue": 23.3},
 {"updatetime": "2017-05-18 21:40:53", "sensorvalue": 22.3},
 {"updatetime": "2017-05-18 22:10:47", "sensorvalue": 20.3},
 {"updatetime": "2017-05-18 22:40:50", "sensorvalue": 20.1},
 {"updatetime": "2017-05-18 23:10:53", "sensorvalue": 20.1},
 {"updatetime": "2017-05-18 23:40:11", "sensorvalue": 20.1}],
 "sensorid": 1}'''
r = r.replace("\r","")
r = r.replace("\n","")
print(r)
data = json.loads(r)
maxs=1
t=1
v_list=data["value"]
print(len(v_list))
for i in range(1,len(v_list)):
    print(i)
    if v_list[i]["sensorvalue"] > v_list[i-1]["sensorvalue"]:
        t += 1
    else:
        t = 1           
    if t >= maxs:
        maxs = t
        p = i
print(p,maxs)
print("温度持续上升的最长时间段是：")
# 列表是从0开始编号的
print("从%s到%s。"%(v_list[p-maxs+1]["updatetime"],v_list[p+1]["updatetime"]))
