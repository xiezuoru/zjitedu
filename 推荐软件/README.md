# 推荐软件

开源硬件的编程软件，推荐教材配套的BXY Editor。BXY Editor同时支持microbit v1、microbit v2和掌控板。

也可以使用mPython软件。

## 相关软件下载

1.BXY Editor


https://bxy.dfrobot.com.cn/download

2.mPython


https://www.mpython.cn/

3.sqliteadmin

一个小巧的Sqlite数据库管理软件。

